import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { ServerlessStack } from '../lib/serverless-api';
import { POLLING_LAMBDA_FUNCTION_NAME } from '../../src/constants';

import * as path from 'path';
import * as dotenv from 'dotenv';
import { HttpMethod } from '@aws-cdk/aws-apigatewayv2';

dotenv.config({ path: path.join(__dirname, '..', '..', '.env') });

const app = new cdk.App();
const lambdaPath = path.join(__dirname, '..', '..', '.lambdas');

new ServerlessStack(
  app,
  'example',
  {
    STACK_SSL_CERTIFICATION_ARN: process.env.STACK_SSL_CERTIFICATION_ARN || '',
    SUB_DOMAIN_NAME: process.env.SUB_DOMAIN_NAME || '',
    DOMAIN_NAME: process.env.DOMAIN_NAME || '',
    lambdaFunctions: [
      {
        functionName: 'auth-lambda',
        codePath: path.join(lambdaPath, 'auth-lambda'),
        handlerName: 'auth-lambda.handler',
        timeout: 15,
        logRetention: 14,
        routePath: '/api/auth',
        methods: [HttpMethod.POST],
      },
      {
        functionName: POLLING_LAMBDA_FUNCTION_NAME,
        codePath: path.join(lambdaPath, 'lambda-polling'),
        handlerName: 'lambda-polling.handler',
        timeout: 60 * 1,
        logRetention: 14,
      },
      {
        functionName: 'graphql',
        codePath: path.join(lambdaPath, 'graphql'),
        handlerName: 'graphql.handler',
        timeout: 15,
        logRetention: 14,
        routePath: '/api/graphql',
        // GET and OPTIONS needed for graphql playground
        methods: [HttpMethod.POST, HttpMethod.OPTIONS, HttpMethod.GET],
      },
    ],
  },
  {
    stackName: 'aws-serverless-stack',
    env: {
      account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
      region: 'us-east-1',
    },
  }
);

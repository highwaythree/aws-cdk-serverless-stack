import * as cdk from '@aws-cdk/core';
import * as acm from '@aws-cdk/aws-certificatemanager';
import * as apigateway from "@aws-cdk/aws-apigatewayv2";
import * as apigatewayIntegrations from "@aws-cdk/aws-apigatewayv2-integrations";
import * as lambda from "@aws-cdk/aws-lambda";
import * as ddb from '@aws-cdk/aws-dynamodb';
import * as iam from "@aws-cdk/aws-iam";
import * as logger from "@aws-cdk/aws-logs";

interface ServerlessStackOptions {
  SUB_DOMAIN_NAME: string
  DOMAIN_NAME: string
  STACK_SSL_CERTIFICATION_ARN: string
  proxyIntegrations?: ProxyIntegration[];
  lambdaFunctions?: LambdaApiProps[];
}
interface ProxyIntegration { 
  url: string;
  path: string;
  methods: apigateway.HttpMethod[] | undefined
}
interface LambdaApiProps {
  functionName: string;
  codePath: string;
  handlerName: string;
  logRetention: logger.RetentionDays;
  routePath?: string;
  timeout?: number;
  methods?: apigateway.HttpMethod[] | undefined
}

export class ServerlessStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, stackOptions: ServerlessStackOptions, props?: cdk.StackProps) {
    /**
     * Force stack to be created in us-east-1 (lambda and cf restriction)
     */
    const AWS_REGION = props?.env?.region || 'us-west-2'
    super(scope, id, 
      {
      ...props, 
      env: { 
        ...props?.env, 
        region: AWS_REGION
      }
    });

    /**
     * Create DynamoDB table
     */
    const ddbTableName = id+'dynamodbTable_0_0_1'
    const ddbTable = new ddb.Table(this, ddbTableName, {
      billingMode: ddb.BillingMode.PAY_PER_REQUEST,
      sortKey: {
        name: 'sk',
        type: ddb.AttributeType.STRING
      } ,     
      partitionKey: {
        name: 'pk',
        type: ddb.AttributeType.STRING
      },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      tableName: ddbTableName,
    })

    new cdk.CfnOutput(this, 'ddbTable', {
      value: ddbTable.tableArn
    });


    /**
     * Policies
     */
    const POLICIES = {
      LAMBDA_EXECUTE_log: new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: ["logs:*"],
        resources: ["arn:aws:logs:*:*:*"]
      }),
      LAMBDA_EXECUTE_s3: new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: [
          "s3:GetObject", 
          "s3:GetObjectTagging", 
          "s3:PutObject",
          "s3:PutObjectTagging",
          "s3:CreateBucket",
          "s3:DeleteBucket",
          "s3:PutBucketTagging"
        ],
        resources: ["arn:aws:s3:::*"]
      }),
      EVENT_POLLING: new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: [
          "events:*"
        ],
        resources: ["arn:aws:events:*:*:*", "arn:aws:sts::*:*"]
      }),
      EVENT_LAMBDA: new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: [
          "lambda:GetFunction",
          "lambda:CreateEventSourceMapping",
          "lambda:AddPermission",
          "lambda:RemovePermission"
        ],
        resources: ["*"]
      }),
      DYNAMO_DB_ACCESS:  new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: [
          "dynamodb:*"
        ],
        resources: [ddbTable.tableArn]
      }),
    }

    /**
     * https certification
     */
    const DOMAIN_NAME = `${stackOptions.SUB_DOMAIN_NAME}.${stackOptions.DOMAIN_NAME}`

    const siteCertificate = acm.Certificate.fromCertificateArn(this, 'site-cert', stackOptions.STACK_SSL_CERTIFICATION_ARN)    
    
    const domainName = new apigateway.DomainName(this, "domain-name", {
      certificate: siteCertificate,
      domainName: DOMAIN_NAME
    });

    /**
     * ApiGateway
     */    
    const httpApi = new apigateway.HttpApi(this, 'httpApi', {
      apiName: 'serverless-stack',
      defaultDomainMapping: {
        domainName
      },
      corsPreflight: {
        allowOrigins: ['*'],
        allowHeaders: ['*', 'Authorization'],
        exposeHeaders: ['*', 'Authorization'],
        allowMethods: [
          apigateway.HttpMethod.OPTIONS, 
          apigateway.HttpMethod.GET, 
          apigateway.HttpMethod.POST, 
          apigateway.HttpMethod.PUT, 
          apigateway.HttpMethod.DELETE]
      },
      createDefaultStage: true
    });

    new cdk.CfnOutput(this, 'httpApiUrl', {
      value: `${httpApi.url}`
    });
    new cdk.CfnOutput(this, 'httpApiEndpoint', {
      value: `${httpApi.apiEndpoint}`
    });
    
    /**
     * API proxy integrations
     */
    stackOptions.proxyIntegrations?.forEach((PI:ProxyIntegration) => {
      const proxyIntegration = new apigatewayIntegrations.HttpProxyIntegration({
        url: PI.url,
      });
      httpApi.addRoutes({
        path: PI.path,
        methods: PI.methods,
        integration: proxyIntegration
      })

      new cdk.CfnOutput(this, `httpApi_proxy-Integration_${PI.path}`, {
        value: `${PI.path} -> ${PI.url}`
      });
    })
    
    /**
     * API lambda function integrations
     */
    const functions: lambda.Function[] = [];
    const functionARNMaps: {[s: string]: string} = {};
    stackOptions.lambdaFunctions?.forEach((lambdaProps: LambdaApiProps) => {

      const handler = new lambda.Function(this, lambdaProps.functionName, {
        runtime: lambda.Runtime.NODEJS_12_X, // So we can use async in widget.js
        code: lambda.Code.fromAsset(lambdaProps.codePath),
        handler: lambdaProps.handlerName,
        timeout: lambdaProps.timeout ? cdk.Duration.seconds(lambdaProps.timeout) : cdk.Duration.seconds(3),
        initialPolicy: [
          POLICIES.LAMBDA_EXECUTE_log,
          POLICIES.LAMBDA_EXECUTE_s3,
          POLICIES.DYNAMO_DB_ACCESS,
          POLICIES.EVENT_POLLING,
          POLICIES.EVENT_LAMBDA
        ],
        logRetention: lambdaProps.logRetention,
        environment: {
          TABLE_NAME_CLIENT: ddbTable.tableName
        },
      });

      
      if (lambdaProps.routePath) {
        const installIntegration = new apigatewayIntegrations.LambdaProxyIntegration({
          handler
        });        
        httpApi.addRoutes({
          path: lambdaProps.routePath,
          methods: lambdaProps.methods,
          integration: installIntegration,
        })
        functions.push(handler)
        new cdk.CfnOutput(this, `httpApi_${lambdaProps.functionName}`, {
          value: `${lambdaProps.routePath} -> ${handler.functionName} >> ${handler.functionArn}`
        });
      } else {
        functionARNMaps[lambdaProps.functionName] = handler.functionArn;
        new cdk.CfnOutput(this, `httpApi_${lambdaProps.functionName}`, {
          value: `No Path -> ${handler.functionName} >> ${handler.functionArn}`
        });
      }
    })
    /*
    * For each function that is used within the api gateway
    * add non api gateway function ARN as environment variable
    */
    functions.forEach(h => {
      Object.keys(functionARNMaps).forEach(key => {
        if (functionARNMaps[key] && h.functionArn !== functionARNMaps[key]){
          console.log('adding lambda function reference', key, functionARNMaps[key])
          h.addEnvironment(`LAMBDA_${key}`, functionARNMaps[key] );
        }
      })      
    })

  }
}
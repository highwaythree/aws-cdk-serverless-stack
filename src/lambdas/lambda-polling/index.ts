import { Context } from 'aws-lambda';
import { removeLambdaPolling } from '../../services/atlassian-service';
import { POLLING_LAMBDA_FUNCTION_NAME } from '../../constants';

export const handler = async (event: any, context: Context) => {
  let client;
  const { taskId, clientId } = event;
  console.log('Within Polling Process');
  await removeLambdaPolling(clientId, taskId, context.invokedFunctionArn);
  return 'polling done';
};

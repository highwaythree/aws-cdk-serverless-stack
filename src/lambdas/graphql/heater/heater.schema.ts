import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './heater.resolver';
export const heaterSchema = makeExecutableSchema({
  typeDefs: `
    type Query {
      ping: String
    }
    type Mutation { 
      poll(pollOptions: String): String
    }
  `,
  resolvers: [resolvers],
});

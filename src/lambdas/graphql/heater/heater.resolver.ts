import { IResolvers } from 'graphql-tools';
import { initiateLambdaPolling } from '../../../services/atlassian-service';

export const resolvers: IResolvers = {
  Query: {
    ping: async (root, args, context) => {
      console.log('pong');
      return 'pong';
    },
  },
  Mutation: {
    poll: async (root, args, context) => {
      console.log('Polling Process Started');
      await initiateLambdaPolling({
        client: {},
        taskId: 'taskid-1',
        pollingData: {},
      });
      return true;
    },
  },
};

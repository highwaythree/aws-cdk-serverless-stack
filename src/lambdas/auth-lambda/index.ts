import { Context, APIGatewayEvent } from 'aws-lambda';
import { jwtSign } from '@services/jwt';
import { EXPIRES_IN } from '../../constants';

export const authFunction = async (clientId: string, jwt: string) => {
  try {
    // forge decode to be true
    const decode = true;
    if (decode) {
      const token = jwtSign({
        data: {},
        expiresIn: EXPIRES_IN,
      });
      console.log('Authtoken', token);
      return {
        token,
        client: {},
        expiresIn: EXPIRES_IN,
      };
    } else {
      throw {
        errorType: 'Unauthorized',
        httpStatus: 401,
        trace: 'Missing Authorization header',
      };
    }
  } catch (err) {
    throw {
      errorType: 'Unauthorized',
      httpStatus: 401,
      trace: err,
    };
  }
};

export const handler = async (event: APIGatewayEvent, context: Context) => {
  const data: any = JSON.parse(event.body);
  const headers = event.headers;
  if (!headers.authorization || !data.baseUrl) {
    return Promise.reject({
      errorType: 'Bad Request',
      httpStatus: 400,
      requestId: context.awsRequestId,
      trace: 'Missing Authorization header',
    });
  }
  const jwt = headers.authorization.replace('JWT ', '');
  const clientId = data.baseUrl.match(/(?<=(https:\/\/|http:\/\/))(.*)/g)[0];
  return authFunction(clientId, jwt)
    .then((res) => {
      return Promise.resolve(res);
    })
    .catch((err) => {
      err.requestId = context.awsRequestId;
      console.log(err);
      return Promise.reject(err);
    });
};

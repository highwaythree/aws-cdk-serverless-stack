import {
  putEventBridgeRule,
  addLambdaPermissions,
  putEventBridgeTarget,
  deleteEventBridgeRule,
  removeEventBridgeTarget,
  removeLambdaPermissions,
} from '@services/aws-cloud-watch';
import { PROJECT_TAG_KEY, POLLING_LAMBDA_FUNCTION_NAME } from '../constants';
export const initiateLambdaPolling = async ({
  client,
  taskId,
  pollingData = {},
  functionName,
}: {
  client: any;
  taskId: string;
  pollingData?: any;
  functionName?: string;
}) => {
  const verbose = true;
  const clientId = 'testing.com';
  const ruleName = `polling-${clientId}-${taskId}`;
  const lambdaStatementId = `polling-${clientId.split('.').join('_')}`;
  const eventBridgeTargetId = `event-bridge-target-id-${clientId}`;
  const rule = await putEventBridgeRule({
    Name: ruleName,
    ScheduleExpression: `rate(2 minutes)`,
    Tags: [
      {
        Key: 'sector',
        Value: `${PROJECT_TAG_KEY}`,
      },
      {
        Key: 'clientId',
        Value: `${clientId}`,
      },
      {
        Key: 'taskId',
        Value: `${taskId}`,
      },
    ],
    Description: `Poll some data`,
  });
  verbose && console.log('polling event rule created', rule);

  const POLLING_LAMBDA_ARN = functionName ? functionName : process.env[`LAMBDA_${POLLING_LAMBDA_FUNCTION_NAME}`];
  verbose && console.log('polling lambda function arn', POLLING_LAMBDA_ARN);
  // const lambda = await getLambdaFunction(POLLING_LAMBDA_ARN);

  const lambdaPermissions = await addLambdaPermissions({
    FunctionName: POLLING_LAMBDA_ARN,
    StatementId: lambdaStatementId,
    Action: 'lambda:InvokeFunction',
    Principal: 'events.amazonaws.com',
    SourceArn: rule.RuleArn,
  });
  verbose && console.log('lambda permissions added', lambdaPermissions);

  const targets = await putEventBridgeTarget({
    Rule: ruleName,
    Targets: [
      {
        Id: eventBridgeTargetId,
        Arn: POLLING_LAMBDA_ARN,
        Input: JSON.stringify({
          clientId,
          taskId,
          pollingData,
        }),
      },
    ],
  });
  verbose && console.log('target added to lambda', targets);
};

export const removeLambdaPolling = async (clientId: string, taskId: string, FunctionArn: string) => {
  const verbose = true;
  const ruleName = `polling-${clientId}-${taskId}`;
  const lambdaStatementId = `polling-${clientId.split('.').join('_')}`;
  const eventBridgeTargetId = `event-bridge-target-id-${clientId}`;

  await removeLambdaPermissions({
    StatementId: lambdaStatementId,
    FunctionName: FunctionArn,
  });
  verbose && console.log('lambda permissions removed');
  await removeEventBridgeTarget({
    Rule: ruleName,
    Ids: [eventBridgeTargetId],
  });
  verbose && console.log('event bridge target removed');
  await deleteEventBridgeRule({
    Name: ruleName,
  });
  verbose && console.log('delete event bridge rule');
};

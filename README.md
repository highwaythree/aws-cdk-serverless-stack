## Install and Build webpack plugin

from the repo root dir

- `cd ./webpack/plugins/lambda-packer`

- `npm install && npm run build`

## Install stack libs

from the repo root dir

- `cd ./stack`
- `npm install`

## Install AWS cli

- `curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"`
- `sudo installer -pkg AWSCLIV2.pkg -target /`
- https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html#cliv2-mac-install-cmd

## Project build and deploy

Theres are bash scripts in the `./scripts` folder for deploying, and destroying your aws stack.

- deploy - `./scripts/deploy.sh`
- destroy - `./scripts/destroy.sh`

Before these commands can be used you will have to

- [configure your aws account](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).
- create `.env` file `cp .env.example`
- You will NEED to set the following `.env` variables;
  - `STACK_SSL_CERTIFICATION_ARN=test`
  - `DOMAIN_NAME=example.com`
